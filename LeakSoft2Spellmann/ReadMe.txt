========================================================================
       DYNAMIC LINK LIBRARY : LeakSoft2Spellmann
========================================================================


Diese LeakSoft2Spellmann-DLL hat der Anwendungs-Assistent f�r Sie erstellt.  

Diese Datei enth�lt eine Zusammenfassung dessen, was Sie in jeder der Dateien
finden, die Ihre LeakSoft2Spellmann-Anwendung bilden.

LeakSoft2Spellmann.dsp
    Diese Datei (Projektdatei) enth�lt Informationen auf Projektebene und wird zur
    Erstellung eines einzelnen Projekts oder Teilprojekts verwendet. Andere Benutzer k�nnen
    die Projektdatei (.dsp) gemeinsam nutzen, sollten aber die Makefiles lokal exportieren.

LeakSoft2Spellmann.cpp
    Dies ist die Hauptquellcodedatei f�r die DLL.

LeakSoft2Spellmann.h
    Diese Datei enth�lt Ihre DLL-Exporte.

/////////////////////////////////////////////////////////////////////////////
Weitere Standarddateien:

StdAfx.h, StdAfx.cpp
    Diese Dateien werden zum Erstellen einer vorkompilierten Header-Datei (PCH) namens
    LeakSoft2Spellmann.pch und einer vorkompilierten Typdatei namens StdAfx.obj verwendet.


/////////////////////////////////////////////////////////////////////////////
Weitere Hinweise:

Der Anwendungs-Assistent verwendet "ZU ERLEDIGEN:", um Bereiche des Quellcodes zu
kennzeichnen, die Sie hinzuf�gen oder anpassen sollten.


/////////////////////////////////////////////////////////////////////////////
