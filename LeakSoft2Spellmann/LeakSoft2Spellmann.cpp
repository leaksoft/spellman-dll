// LeakSoft2Spellmann.cpp : Definiert den Einsprungpunkt f�r die DLL-Anwendung.
//

#include "stdafx.h"
#include <stdio.h>
#include "LeakSoft2Spellmann.h"
/************************************************
	25.07.2016 Erweiterung um Prozess, der Telegramme des Generators bearbeitet und quittiert
************************************************/
HANDLE m_hCom;

#define	RequestInputChar	ETX
#define	Rx_TimeBase_ms		10
#define	RXBUFLEN			100

DWORD	Quit_Timeout_ms	=	500;
DWORD	Read_Timeout_ms	=	2000;
DWORD	Number_Versuche	=	3;
DWORD	BaudRate;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			m_hCom = INVALID_HANDLE_VALUE;
			if ((m_hEvent = CreateEvent(NULL,FALSE,TRUE,NULL))==NULL) return FALSE;
			_beginthread(MyThreadProc,0, NULL);
			break;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			if (running)
			{
				ende = true;
				SetEvent(m_hEvent);
				Sleep(50);
			}
			if (m_hCom != INVALID_HANDLE_VALUE) CloseHandle(m_hCom);
			m_hCom = INVALID_HANDLE_VALUE;
			break;
    }
    return TRUE;
}

LEAKSOFT2SPELLMANN_API char RxTelegramm[100];
LEAKSOFT2SPELLMANN_API char TxTelegramm[100];
LEAKSOFT2SPELLMANN_API char RxTelegramm1[100];
LEAKSOFT2SPELLMANN_API char RxTelegramm2[100];
LEAKSOFT2SPELLMANN_API char RxTelegramm3[100];

DWORD nBufferStart;
DWORD nBytesRead;
char buffer[RXBUFLEN+10];
void ClearCom()
{
	nBufferStart = 0;
	if (m_hCom!=INVALID_HANDLE_VALUE)
	{
		DWORD e;
		COMSTAT s;
		ClearCommError(m_hCom,&e,&s);
		if (s.cbInQue) 
		{
			DWORD nBytesRead;
			if (ReadFile(m_hCom,&buffer,RXBUFLEN,&nBytesRead,NULL))// clear input queue
			{}
		}
		PurgeComm(m_hCom,PURGE_RXCLEAR);
	}
}
extern int EmpfStr(LPSTR rbuffer,bool mitquit=true);
extern bool SendStr(LPCSTR str,bool noquit=false);
int EmpfStr(LPSTR rbuffer,bool mitquit)
{
	BOOL ready=FALSE;
	DWORD ExitTicks=GetTickCount()+(mitquit?Read_Timeout_ms:Quit_Timeout_ms);
	rbuffer[0] = 0;
	do
	{
		buffer[nBufferStart] = 0;
		char*psyn=strstr(buffer,PreTelegramm);
		if (psyn)
		{
			if (psyn>buffer) 
			{//characters vor PreTelegramm l�schen
				nBufferStart -= psyn-buffer;
				memcpy(buffer,psyn,nBufferStart+1);
			}
			char*petx = strchr(buffer,ETX);
			if (petx)
			{
				if (petx==&buffer[nBufferStart-1])
				{//ETX ist letztes geselenes Zeichen jetzt noch Checksumme lesen
					if(!ReadFile(m_hCom,&buffer[nBufferStart],1,&nBytesRead,NULL)) return false;// error or timeout
					nBufferStart += nBytesRead;
					buffer[nBufferStart] = 0;
				}
				if (petx!=&buffer[nBufferStart-1])
				{//vollst�ndiges Telegramm im buffer
					unsigned char cs=0;
					int buflen = petx-&buffer[4];
					for (char*p=buffer+4;p!=(petx+2);p++) cs ^= (unsigned char)*p;
					if (cs==0)
					{
						strncpy(rbuffer,buffer+4,buflen);
						rbuffer[buflen]=0;
					}
					else buflen = -1;
					nBufferStart -= (petx-buffer)+2;
					memcpy(buffer,petx+2,nBufferStart+1);
					if (mitquit) SendStr(buflen<0?NAKSTR:ACKSTR,true);
					return buflen;
				}
			}
			else if (nBufferStart>(RXBUFLEN-10))
			{
				nBufferStart -= 4;
				memcpy(buffer,&buffer[4],nBufferStart+1);

			}
		}
		else if (nBufferStart>3)
		{
			memcpy(buffer,&buffer[nBufferStart-3],4);//delete buffer bis auf 3 character, welche ein teil des PreTelegramms sein k�nnten
			nBufferStart = 3;
		}
		if(!ReadFile(m_hCom,&buffer[nBufferStart],RXBUFLEN-nBufferStart,&nBytesRead,NULL)) return 0;// error or timeout
		if (nBytesRead)
		{
			nBufferStart += nBytesRead;
			buffer[nBufferStart] = 0;
		}
	}
	while(((int)(ExitTicks-GetTickCount()))>0);
	return 0;
}
char* StoreStr(char*dest,char*source,DWORD len)
{
	if (len>30) len = 30;
	for (DWORD i=0;i<len;i++,dest += 3) sprintf(dest,"%02X ",(unsigned char)source[i]);
	return dest;
}
bool SendStr(LPCSTR str,bool noquit)
{
	char buffer[400];
	ClearCom();
	DWORD nBytesWritten;
	unsigned char cs=ETX;
	for (unsigned int i=0;str[i];i++) cs ^= (unsigned char)str[i];
	sprintf(buffer,PreTelegramm "%s\x03%c",str,cs);
	if (!noquit) strcpy(TxTelegramm,str);
	for (DWORD versuche=0;versuche<Number_Versuche;versuche++)
	{
		if (WriteFile(m_hCom,buffer,strlen(str)+6,&nBytesWritten,NULL)&&(nBytesWritten==(strlen(str)+6)))
		{
			if (noquit) return true;
			char rxbuf[RXBUFLEN+1];int rxlen=EmpfStr(rxbuf,false);
			if ((rxlen==1)&&(rxbuf[0]==ACK))
			{
				return true;
			}
		}
	}
	return false;
}

LEAKSOFT2SPELLMANN_API char* fnLeakSoft2Spellmann(char*Befehl)
{
	*TxTelegramm = 0;
	*RxTelegramm = 0;
	*RxTelegramm1 = 0;
	*RxTelegramm2 = 0;
	*RxTelegramm3 = 0;
	static char Param[20];
	char*retval=KOMMUNIKATIONSFEHLER;
	if (Befehl==NULL) return BEFEHLSFEHLER;
	WaitForSingleObject(m_hEvent,INFINITE);
	switch (Befehl[0])
	{
	case 'C':
		/*
		3.1 Kommunikationsparameter
		3.1.1 Com-Port
		Befehl aus LeakSoft
		Cxx xx -> Com-Port
		Beispiel: COM2 -> C02
		COM15 -> C15
		*/
		if (Befehl[1]=='Q')
		{	int n;char c;
			if ((sscanf(Befehl+2,"%u%c",&n,&c)==1)&&(n>=50)&&(n<=1000))
			{
				Quit_Timeout_ms = n;
				retval = OK;
			}
			else retval = PARAMETERFEHLER;
		}
		else if (Befehl[1]=='R')
		{	int n;char c;
			if ((sscanf(Befehl+2,"%u%c",&n,&c)==1)&&(n>=100)&&(n<=5000))
			{
				Read_Timeout_ms = n;
				retval = OK;
			}
			else retval = PARAMETERFEHLER;
		}
		else if (Befehl[1]=='N')
		{	int n;char c;
			if ((sscanf(Befehl+2,"%u%c",&n,&c)==1)&&(n>=1)&&(n<=10))
			{
				Number_Versuche = n;
				retval = OK;
			}
			else retval = PARAMETERFEHLER;
		}
		else
		{
			unsigned int n;char c;
			if ((sscanf(Befehl+1,"%u%c",&n,&c)==1)&&(n<100))
			{
				if (m_hCom != INVALID_HANDLE_VALUE) CloseHandle(m_hCom);
				m_hCom = INVALID_HANDLE_VALUE;
				if ((n>0)&&(n<100))
				{
					char Name[20];
					sprintf(Name,"\\\\.\\com%u",n);
					if((m_hCom = CreateFile(Name,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL)) !=  INVALID_HANDLE_VALUE)
					{
						DCB dcb;
						if(GetCommState(m_hCom,&dcb))
						{
							if (BaudRate) dcb.BaudRate = BaudRate;
							else BaudRate = dcb.BaudRate;
							dcb.fBinary = TRUE;
							dcb.fParity = FALSE;
							dcb.fOutxCtsFlow = FALSE;
							dcb.fOutxDsrFlow = FALSE;
							dcb.fDtrControl = DTR_CONTROL_ENABLE;
							dcb.fDsrSensitivity = FALSE;
							dcb.fTXContinueOnXoff = FALSE;
							dcb.fOutX = FALSE;
							dcb.fInX = FALSE;
							dcb.fErrorChar = FALSE;
							dcb.fNull = FALSE;
							dcb.fRtsControl = RTS_CONTROL_ENABLE;
							dcb.fAbortOnError = FALSE;
							dcb.ByteSize = 8;
							dcb.Parity = NOPARITY;
							dcb.StopBits = TWOSTOPBITS;
							dcb.EvtChar = RequestInputChar;
							if(SetCommState(m_hCom,&dcb))
							{
									COMMTIMEOUTS cto;

									// Read kehrt mit allen Zeichen des Puffers zur�ck, wenn mindestens ein Zeichen im Puffer war 
									// oder  nach dem Timeout (Rx_TimeBase_ms)

									if(GetCommTimeouts(m_hCom,&cto))
									{
										cto.ReadIntervalTimeout = MAXDWORD;
										cto.ReadTotalTimeoutMultiplier = MAXDWORD;
										cto.ReadTotalTimeoutConstant = Rx_TimeBase_ms;
										//cto.WriteTotalTimeoutMultiplier = ;
										// cto.WriteTotalTimeoutConstant = ;
										if(SetCommTimeouts(m_hCom,&cto))
										{
											retval = OK;
											break;
										}
									}
							}
						}
						CloseHandle(m_hCom);
						m_hCom = INVALID_HANDLE_VALUE;
					}
				}
			}
			else retval = PARAMETERFEHLER;
		}
		break;
	case 'B':
		/*
		3.1.2 Baudrate
		Befehl aus LeakSoft
		Bxxxxx xxxxxx -> Baut Rate 4800 � 38400
		Beispiel: 4800 -> B04800
		38400 -> B38400
		*/
		{
			DWORD br;char c;
			DCB dcb;
			if ((m_hCom !=  INVALID_HANDLE_VALUE)&&(sscanf(Befehl+1,"%u%c",&br,&c)==1)&&GetCommState(m_hCom,&dcb))
			{
				dcb.BaudRate = br;
				if(SetCommState(m_hCom,&dcb))
				{
					BaudRate = dcb.BaudRate;
					retval = OK;
				}
				else retval = PARAMETERFEHLER;
			}
		}
		break;
	case 'M':
		/*
		3.2 Parameter einstellen
		Befehl aus LeakSoft
		Mxy x -> Device
		y -> Fokus (S -> klein; L -> gro�)
		Beispiel: Device 5, Focus klein -> M5S
		Device 1, Focus gro� -> M1L
		Befehl an Generatori
		CDGxy x = Device (0 � 6)
		Y = Focus (k -> small focus; g -> large focus)
		*/
		if (m_hCom != INVALID_HANDLE_VALUE)
		{
			char device,fokus,c;
			if ((sscanf(Befehl+1,"%c%c%c",&device,&fokus,&c)==2)&&(device>='0')&&(device<='6')&&((fokus=='S')||(fokus=='L')))
			{
				sprintf(Param,"CDG%c%c",device,(fokus=='S'?'k':'g'));
				if (SendStr(Param))
				{
					int rlen;
					int isok=0;
					retval = OK;//PARAMETERFEHLER;
					strcpy(Param,"M  ");
					while (rlen=EmpfStr(RxTelegramm))
					{
						if ((rlen>0)&&(strstr(RxTelegramm,"CDG")==RxTelegramm))
						{
							if (isdigit(RxTelegramm[3]))
							{
								strcpy(RxTelegramm1,RxTelegramm);
								isok |= 1;
								if (RxTelegramm[3]!=device) isok |= 8;
								Param[1]=RxTelegramm[3];
							}
							else if ((RxTelegramm[3]|0x20)=='k')
							{
								isok |= 2;
									if (fokus!='S') isok |= 0x10;
									Param[2] = 'S';
									strcpy(RxTelegramm2,RxTelegramm);
							}
							else if ((RxTelegramm[3]|0x20)=='g')
							{
									if (fokus=='S') isok |= 0x10;
									isok |= 2;
									Param[2] = 'L';
									strcpy(RxTelegramm2,RxTelegramm);
							}
							else
							{
								isok |= 4;
								strcpy(RxTelegramm3,RxTelegramm);
							}
						}
					}
					if ((isok&3) == 3) retval = Param;
				}
			}
			else retval = PARAMETERFEHLER;
		}
		break;
	case 'U':
		/*
		3.3 Einstellen der R�hrenspannung
		Befehl aus LeakSoft
		Uxxx xxx -> Spannungsangabe in KV
		Beispiel: 70kV -> U070
		150kV -> U150
		Befehl an Generatorii
		CDI[35 � 150]) Spannung 35kV bis 150kV
		*/
		if (m_hCom != INVALID_HANDLE_VALUE)
		{
			unsigned int voltage;char c;
			if ((sscanf(Befehl+1,"%u%c",&voltage,&c)==1)&&(voltage>=35)&&(voltage<=150))
			{
				sprintf(Param,"CDI%u",voltage);
				if (SendStr(Param))
				{
					int rlen;
					retval = OK;//SPANNUNGSFEHLER;
					while (rlen=EmpfStr(RxTelegramm))
					{
						if ((rlen>0)&&(strstr(RxTelegramm,"CDI")==RxTelegramm))
						{
							unsigned int istvoltage;
							strcpy(RxTelegramm1,RxTelegramm);
							if (sscanf(RxTelegramm+3,"%u%c",&istvoltage,&c)==1)
							{
								sprintf(Param,"U%03u",istvoltage);
								retval = Param;
							}
						}
					}
				}
			}
			else retval = PARAMETERFEHLER;
		}
		break;
	case 'I':
		/*
		3.4 Einstellen des R�hrenstroms
		Befehl aus LeakSoft
		Ixxx.xx xxx.xx -> Stromangabe in mA
		Beispiel: 5mA -> I005.00
		2,5mA -> I002.50
		Befehl an Generatoriii
		CDJ[1.0 � 5.0] Strom 1,0mA bis 5,0mA
		*/
		if (m_hCom != INVALID_HANDLE_VALUE)
		{
			int j;
			float current;char c;
			if (((j=sscanf(Befehl+1,"%f%c",&current,&c))==1)&&(current>=1.0)&&(current<=5.0))
			{
				sprintf(Param,"CDJ%1.1f",current);
			}
			else if ((j==1)&&(current>5.0)&&(current<=10.0))
			{
				sprintf(Param,"CDJ%1.0f",current);
			}
			else if ((j==1)&&(current>=50.0)&&(current<=300.0))
			{
				sprintf(Param,"CDJ%2.0f",current);
			}
			else
			{
				retval = PARAMETERFEHLER;
				break;
			}
			if (SendStr(Param))
			{
				int rlen;
				retval = OK;//STROMFEHLER;
				while (rlen=EmpfStr(RxTelegramm))
				{
					if ((rlen>0)&&(strstr(RxTelegramm,"CDJ")==RxTelegramm))
					{
						float istcurrent;
						strcpy(RxTelegramm1,RxTelegramm);
						if ((sscanf(RxTelegramm+3,"%f%c",&istcurrent,&c)==1))
						{
							sprintf(Param,"I%06.2f",istcurrent);
							retval = Param;
						}
					}
				}
			}
		}
		break;
	case 'T':
		/*
		3.5 Strahldauer
		Befehl aus LeakSoft
		Txxx xxx -> Strahldauer in Sekunden
		Beispiel: 5 Sek. -> T005
		70 Sek. -> T070
		Befehl an Generatoriv
		CDK[0 � 5999] Strahlzeit 0 bis 5999 Sekunden
		*/
		if (m_hCom != INVALID_HANDLE_VALUE)
		{
			unsigned int time;char c;
			if ((sscanf(Befehl+1,"%u%c",&time,&c)==1)&&(time>=0)&&(time<=5999))
			{
				sprintf(Param,"CDK%u",time);
				if (SendStr(Param))
				{
					int rlen;
					retval = OK;//STRAHLZEITSETZENFEHLER;
					while (rlen=EmpfStr(RxTelegramm))
					{
						if ((rlen>0)&&(strstr(RxTelegramm,"CDK")==RxTelegramm))
						{
							unsigned int isttime;
							strcpy(RxTelegramm1,RxTelegramm);
							if (sscanf(RxTelegramm+3,"%u%c",&isttime,&c)==1)
							{
								sprintf(Param,"T%03u",isttime);
								retval = Param;
							}
						}
					}
				}
			}
			else retval = PARAMETERFEHLER;
		}
		break;
	case 'R':
		/*
		3.6 Reset der Strahlzeit (Timer)
		Befehl aus LeakSoft
		RES
		Befehl an Generatorv
		TE45 Resettaste dr�cken
		TA45 Resettaste loslassen
		*/
		if (m_hCom != INVALID_HANDLE_VALUE)
		if ((strcmp(Befehl,"RES")==0))
		{
			if (SendStr("CTE,07"))
			{
				retval = STRAHLZEITRESETFEHLER;
				while (EmpfStr(RxTelegramm)){}
				if (SendStr("CTA,07"))
				{
					retval = OK;
					while (EmpfStr(RxTelegramm)){}
				}
			}
		}
		else UNBEKANNTER_BEFEHL;
		break;
	default:retval = UNBEKANNTER_BEFEHL;
	}
	SetEvent(m_hEvent);
	return retval;
}

HANDLE m_hEvent;
bool ende,running;

void __cdecl MyThreadProc( LPVOID pParam )
{
    running = true;
	while ((WaitForSingleObject(m_hEvent,INFINITE)!=WAIT_FAILED)&&!ende)
	{
		if (m_hCom!=INVALID_HANDLE_VALUE)
		{
			COMSTAT s;
			DWORD e;
			if (!nBufferStart) ClearCommError(m_hCom,&e,&s);
			if (nBufferStart||s.cbInQue) 
			{
				char rxbuf[RXBUFLEN+1];DWORD rxlen=EmpfStr(rxbuf);
				if (rxlen>0)
				{
					if (strstr(rxbuf,"CV")==rxbuf) SendStr(rxbuf);//echo "CV" Telegramme
				}
			}
		}
		SetEvent(m_hEvent);
		Sleep(0);
	}
	running = false;
}

