
// Der folgende ifdef-Block zeigt die Standardl�sung zur Erstellung von Makros, die das Exportieren 
// aus einer DLL vereinfachen. Alle Dateien in dieser DLL wurden mit dem in der Befehlszeile definierten
// Symbol LEAKSOFT2SPELLMANN_EXPORTS kompiliert. Dieses Symbol sollte f�r kein Projekt definiert werden, das
// diese DLL verwendet. Auf diese Weise betrachtet jedes andere Projekt, dessen Quellcodedateien diese Datei 
// einbeziehen, LEAKSOFT2SPELLMANN_API-Funktionen als aus einer DLL importiert, w�hrend diese DLL mit diesem 
// Makro definierte Symbole als exportiert betrachtet.
extern "C"
{
#ifdef LEAKSOFT2SPELLMANN_EXPORTS
#define LEAKSOFT2SPELLMANN_API __declspec(dllexport)
#else
#define LEAKSOFT2SPELLMANN_API __declspec(dllimport)
#endif

LEAKSOFT2SPELLMANN_API char* fnLeakSoft2Spellmann(char*Befehl);

extern LEAKSOFT2SPELLMANN_API char RxTelegramm[100];
extern LEAKSOFT2SPELLMANN_API char TxTelegramm[100];
extern LEAKSOFT2SPELLMANN_API char RxTelegramm1[100];
extern LEAKSOFT2SPELLMANN_API char RxTelegramm2[100];
extern LEAKSOFT2SPELLMANN_API char RxTelegramm3[100];
}
/*
E1 Kommunikationsfehler
E2 Parameter konnten nicht gesetzt werden
E3 R�hrenspannung kon76nte nicht gesetzt werden
E4 R�hrenstrom konnte nicht gesetzt werden
E5 Strahlzeit konnte nicht gesetzt werden
E6 Strahlzeit konnte nicht zur�ckgesetzt werden
*/
#define	OK						NULL
#define	KOMMUNIKATIONSFEHLER	"E1"
#define	PARAMETERFEHLER			"E2"
#define	SPANNUNGSFEHLER			"E3"
#define	STROMFEHLER				"E4"
#define	STRAHLZEITSETZENFEHLER	"E5"
#define	STRAHLZEITRESETFEHLER	"E6"
#define	BEFEHLSFEHLER			"E7"
#define	UNBEKANNTER_BEFEHL		"E8"

#define	STX						0x02//02 hex stx - start of text
#define	ETX						0x03//03 hex etx - end of text
#define	ACK						0x06//06 hex ack � acknowledge
#define	NAK						0x15//15 hex nak - not acknowledge
#define	SYN						0x16//16 hex syn - synchronisation
#define	ACKSTR					"\x06"
#define	NAKSTR					"\x15"
#define	PreTelegramm			"\x16\x16\x16\x02"

extern HANDLE m_hEvent;
extern bool ende,running;
void __cdecl MyThreadProc( LPVOID pParam );
